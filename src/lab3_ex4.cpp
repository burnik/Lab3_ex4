#include <iostream>
#include <vector>

#include "TUIlist.hpp"
#include "TUImenu.hpp"
#include "Plant.hpp"
#include "PlantsArray.hpp"

using namespace std;

/*
    Tasks:
        Добавление элемента в произвольное место массива
        Просмотр массива в прямом и обратном направлении (Vector.reverse() as arg)
        Удаление произвольного элемента массива
*/

void addElement(PlantsArray *data)
{
    int userInput = 0;

    cout << "0) Назад" << endl
         << "1) Добавить элемент в начало списка" << endl
         << "2) Добавить элемент в конец списка" << endl
         << "3) Добавить элемент в произвольное место" << endl
         << "> ";

    cin >> userInput;

    //Проверяем корректность ввода
    if (userInput < 0 || userInput > 3)
    {
        cout << "Некорректный ввод" << endl;
        return;
    }

    //Выходим
    if (!userInput)
        return;

    switch (userInput)
    {
        //Запись в начало списка
        case 1:
            data->addElementFromKeyboard(POS_FIRST);
        break;

        //Запись в конец списка
        case 2:
            data->addElementFromKeyboard(POS_LAST);

        break;

        //Запись в произвольное место
        case 3:
            int buf;
            cout << "Индекс для вставки элемента> ";
            cin >> buf;
            data->addElementFromKeyboard(buf);
        break;

    }

}

//Печатает растения начиная
void printFilterByMonth(PlantsArray *arrObj)
{
    int fromMonth, toMonth;

    cout << "Начальный месяц> ";
    cin >> fromMonth;

    cout << "Начальный месяц> ";
    cin >> toMonth;

    auto filteredPlants = arrObj->getFilterMonth(fromMonth, toMonth);

    TUIlist<Plant>(filteredPlants);

}

//Записать массив в файл
void writeToFile(PlantsArray *arrObj)
{
    string fileName;
    cout << "Название файла> ";
    cin >> fileName;

    arrObj->writeToFile(fileName);
}

void changePriceByName(PlantsArray *arr)
{
    char name[NAME_MAX_SIZE];
    cout << "Введите название> ";
    cin >> name;

    int newPrice;
    cout << "Введите новую цену> ";
    cin >> newPrice;

    arr->changePriceByName(name, newPrice);
}

void showArray(PlantsArray *arr)
{
    TUIlist<Plant>(arr->getData());
}

void readFromFile(PlantsArray *arrObj)
{
    string fileName;
    cout << "Название файла> ";
    cin >> fileName;

    arrObj->readFromFile(fileName);
}


int main()
{
    /*
    Четвертое задание предполагает создание информационно-
    справочной системы на базе бинарного файла записей со следующими
    возможностями: создание файла, просмотр содержимого файла, добавление,
    удаление и корректировка данных, а также выполнение запросов в
    соответствии с заданием. Поиск требуемых данных осуществлять по
    ключевому полю. Для организации интерфейса должно использоваться меню.
    */

    PlantsArray arr;

    //Задаём меню
    TUImenu::arg = &arr;

    //Задаём элементы
    TUImenu::addElement(readFromFile,       "Прочитать из файла");
    TUImenu::addElement(writeToFile,        "Записать в файл");
    TUImenu::addElement(addElement,         "Добавить элемент в массив");
    TUImenu::addElement(showArray,          "Вывести массив");
    TUImenu::addElement(printFilterByMonth, "Семена которые можно высаживать с марта по май");
    TUImenu::addElement(changePriceByName,  "Изменить цену семян по его названию");
    TUImenu::start();

    return 0;
}
