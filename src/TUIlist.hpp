#ifndef TUILIST_H
#define TUILIST_H


#include <vector>
#include <iostream>
#include <string>
#include <cctype>
#include <math.h>


#if defined(_WIN32)
#define WIN32_LEAN_AND_MEAN
#define VC_EXTRALEAN
#include <Windows.h>
#elif defined(__linux__)
#include <sys/ioctl.h>
#endif // Windows/Linux


//Defines
#define PRINTSIZE_AUTO 0	//Автоматически определить количество эл. в выводе


using namespace std;

/*
	Получает размеры терминала
	Автор: https://stackoverflow.com/users/9178992/projectphysx
	Ссылка https://stackoverflow.com/questions/23369503/get-size-of-terminal-window-rows-columns/62485211#62485211
*/
void get_terminal_size(int& width, int& height)
{
	#if defined(_WIN32)
		CONSOLE_SCREEN_BUFFER_INFO csbi;
		GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
		width = (int)(csbi.srWindow.Right-csbi.srWindow.Left+1);
		height = (int)(csbi.srWindow.Bottom-csbi.srWindow.Top+1);
	#elif defined(__linux__)
		struct winsize w;
		ioctl(fileno(stdout), TIOCGWINSZ, &w);
		width = (int)(w.ws_col);
		height = (int)(w.ws_row);
	#endif // Windows/Linux
}

template <typename T>
void TUIlist(vector<T> data, size_t printSize = PRINTSIZE_AUTO)
{

	//Размеры терминала, определяют количество элементво для вывода
	int terminalWidth, terminalHeight;
	
	//Определяем нужно ли определять размеры терминала
	if (printSize == PRINTSIZE_AUTO)
		get_terminal_size(terminalWidth, terminalHeight);
	else
	{
		terminalWidth 	= 60;
		terminalHeight 	= printSize;
	}
	
	//Проверяем, нужно ли выводить меню
    if (data.size() <= (size_t)(terminalHeight))
    {
        for (size_t i = 0; i < data.size(); i++)
            cout << data[i] << endl;

        cout << "Конец файла. Введите любой текст и нажмите Enter..." << endl;
        string buf;
		cin >> buf;
        return;
    }

	size_t lastElement 		= 0,					//Индекс полседнего эл.
		   elementsShowOnce	= terminalHeight - 2;	//Количество элементов за
													//один показ
	
	//Символ для получения команды от пользователя
    char inputStr = '\0';
    
    size_t endElement = 0;

	//Main-loop в показе данных
    do
    {
		get_terminal_size(terminalWidth, terminalHeight);
		elementsShowOnce = terminalHeight - 2;

		//Выводим номер страницы
    	cout << "-==Страница: " << lastElement / elementsShowOnce + 1 << " из "
    		 << ceil((double)data.size() / (double)elementsShowOnce) << "==-" << endl;
    	
    	if (lastElement >= data.size() - elementsShowOnce)
    		endElement = data.size();
    	else
    		endElement = lastElement + elementsShowOnce;


		//Выводим элементы
		for (size_t i = lastElement; i < endElement; i++)
			cout << i << ") " << data[i] << endl;
		
		//Выводим информационное окно
		if (lastElement < data.size() - elementsShowOnce)
			cout << "N - след.; ";
		if (lastElement > 0)
			cout << "P - пред.; ";
		cout << "Q - выход>";
		cin >> inputStr;

    	switch(tolower(inputStr))
    	{
    		//Next
    		case 'n':
    			if (lastElement < endElement - elementsShowOnce + 1)
					lastElement += elementsShowOnce;

    		break;

			//Previous
    		case 'p':
    			if (lastElement >= elementsShowOnce)
					lastElement -= elementsShowOnce;
				else
					lastElement = 0;

    		break;
    	}

    }while(tolower(inputStr) != 'q');
}

#endif
