#ifndef TUIMENU_H
#define TUIMENU_H

#include <iostream>
#include <vector>
#include <string>
#include <any>

#include "PlantsArray.hpp"

using namespace std;

//TODO: Сделать структуру method классом, с перегрузкой метода "<<"

struct method
{
	void (*p)(PlantsArray*);
	string name;
};

class TUImenu
{
	public:
		static void addElement(void (*methodP)(PlantsArray*), string methodName)
		{
			method buf;
			buf.p 		= methodP;
			buf.name 	= methodName;

			methods.push_back(buf);
		};

		static inline PlantsArray *arg;

		static void start()
		{
			//Ввод пользователя
			int userInput = 0;

			do
			{
				cout << "0) Выйти" << endl;
				for (size_t i = 0; i < methods.size(); i++)
					cout << i + 1 << ") " << methods[i].name << endl;

				//Ввод номера метода
				cout << "> ";
				cin >> userInput;

				//Выбран выход из программы
				if (!userInput)
				{
					cout << "(´^ω^)ノ"  <<  endl;
					break;
				}

				TUImenu::clearScreen();

				//Проверяем существует ли метод с таким номером
				if ((size_t)userInput > methods.size() || userInput < 0)
				{
					cout << "Ошибка при вводе номера метода" << endl;
					continue;
				}

				//Вызываем пользовательский метод
				methods[userInput-1].p(TUImenu::arg);

				cout << endl;

			}while (userInput);


		};


    private:
    	static void clearScreen()
		{
			#ifdef WINDOWS
			//Win
			system("cls");
			#else

			//POSIX-like
			system("clear");
			#endif
		}

		//Вектор методов функции
		//https://habr.com/ru/company/otus/blog/561772/
      	static inline vector<method> methods;
    	TUImenu() {};

};

#endif
