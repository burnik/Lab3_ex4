#ifndef PLANTSARRAY_H
#define PLANTSARRAY_H

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

#include "Plant.hpp"

//inline - ?

#define POS_FIRST 0	//Первый элемент
#define POS_LAST -1	//Последний элемент

//Выбор поля сортировки
#define SORT_NAME 0
#define SORT_MONTH 1
#define SORT_PRICE 2
#define SORT_AMOUNT 3

using namespace std;

//Класс "Массив"
class PlantsArray
{
public:
	PlantsArray();
	void addElement(Plant, int);		//Добавить элемент
	void deleteElement(Plant, int);		//Удалить элемент

	//Добавить элемент в массив с клавиатуры
	void addElementFromKeyboard(int);

	void show();	//Вывод массива

	vector<Plant> getFilterMonth(short, short);

	void changePriceByName(char*, int);


	inline vector<Plant> getData(){return data;};
	inline void			 setData(vector<Plant>);

	//Чтение и запись в файл
	void writeToFile(string);
	void readFromFile(string);

private:
	vector<Plant> data;

};

#endif
