#ifndef PLANT_H
#define PLANT_H

#include <iostream>	//Для перегрузки "<<" в файле реализации
#include <string.h>
#include <iomanip>

//inline - ?

#define NAME_MAX_SIZE 32

using namespace std;

class Plant
{
public:
	Plant(char*, short, unsigned int, unsigned int);
	Plant();
	char* getName();
	void setName(char*);

	void setPrice(int);

	short getMonth();

	friend ostream& operator<<(ostream&, const Plant&);

private:
	char name[NAME_MAX_SIZE];	//Название растения
	short month;				//Месяц посадки

	unsigned int price,		//Цена за упаковку
				 amount;	//Количество семян в упаковке

};

#endif
