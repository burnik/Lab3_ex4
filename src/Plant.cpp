#include "Plant.hpp"

Plant::Plant()
{
}

Plant::Plant(char plantName[NAME_MAX_SIZE], short plantMonth, unsigned int plantPrice, unsigned int plantAmount)
{
    strcpy(name, plantName);
    month = plantMonth;
    price = plantPrice;
    amount = plantAmount;
}
//
ostream& operator<<(ostream& os, const Plant& dt)
{
    cout << "Название: " << setw(16) << dt.name << "; Месяц: " << setw(3) << dt.month
         << "; Цена: " << setw(5) << dt.price << "; Кол-во: " << setw(3) << dt.amount;
    return os;
}

short Plant::getMonth()
{
    return this->month;
}

void Plant::setName(char * newName)
{
    strcpy(this->name, newName);
}

void Plant::setPrice(int newPrice)
{
    this->price = newPrice;
}

char* Plant::getName()
{
    return this->name;
}
