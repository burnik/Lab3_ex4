#include "PlantsArray.hpp"

PlantsArray::PlantsArray()
{
}

void PlantsArray::writeToFile(string fileName)
{
    vector<Plant> plants = this->getData();

    ofstream fout(fileName, ios::out | ios::binary);

    fout.write((char*)&plants[0], plants.size() * sizeof(Plant));
    fout.close();

}

void PlantsArray::addElementFromKeyboard(int pos)
{
    char name[NAME_MAX_SIZE];
    cout << "Название растения> ";
    cin >> name;

    short month;
    cout << "Месяц посадки [1-12]> ";
    cin >> month;

    unsigned int price;
    cout << "Цена растения> ";
    cin >> price;

    unsigned int amount;
    cout << "Количество семян в упаковке> ";
    cin >> amount;

    Plant newPlant(name, month, price, amount);
    cout << "No error\n";

    this->addElement(newPlant, pos);
}

vector<Plant> PlantsArray::getFilterMonth(short monthFrom, short monthTo)
{
    vector<Plant> buf = this->getData();

    vector<Plant> output;

    copy_if(buf.begin(), buf.end(),
             back_inserter(output),
             [=](Plant pl)  //mutable -?
             {
                 short month = pl.getMonth();
                 return (month >= monthFrom) && (month <= monthTo);
             });

    return output;
}

void PlantsArray::changePriceByName(char *name, int newPrice)
{
    for (size_t i = 0; i < this->data.size(); i++)
    {
        if (!strcmp(name, this->data[i].getName()))
            this->data[i].setPrice(newPrice);
    }

}

void PlantsArray::addElement(Plant newPlant, int pos)
{
    if (pos < -1)
        return;

    if (pos == POS_LAST || pos > this->data.size())
        this->data.push_back(newPlant);
    else
        this->data.insert(data.begin() + pos, newPlant);
}

void PlantsArray::readFromFile(string fileName)
{
    vector<Plant> output;

    //Открываем файл для чтения
    ifstream input(fileName, ios::binary);

    //Произошла ошибка при открытии файла, возможно, он не существует.
    if (input.fail())
        return;

    //Читаем данные из файла пока это возможно
    Plant buffer;
    while (input.read((char *)&buffer, sizeof(Plant)))
    {
        if (!input.good())
        {
            string bufStr = "";
            cout << "Произошла ошибка при чтении. Возможно, файл повреждён." << endl
                 << "Продолжить чтение? [y/N]>";
            cin >> bufStr;

            if (tolower(bufStr[0]) == 'y')
                continue;

            break;
        }

        output.push_back(buffer);
    }

    //Закрываем файл
    input.close();

    cout << "!!!" << output.size() << "!!!" << endl;

    for (int i = 0; i < output.size(); i++)
        cout << output[i] << endl;

    this->data = output;
}

inline void PlantsArray::setData(vector<Plant> myData)
{
    //Вот тут возможно чот не будет работать, inline/this
    this->data = myData;
}

