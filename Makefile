all: lab3_ex4

WARNINGS = -Wall
DEBUG = -ggdb -fno-omit-frame-pointer
OPTIMIZE = -O0
CPP_VERSION = c++20
CC = g++

SOURCES = src/lab3_ex4.cpp src/TUIlist.hpp src/TUImenu.hpp src/Plant.hpp src/Plant.cpp src/PlantsArray.hpp src/PlantsArray.cpp


lab3_ex4: $(SOURCES)
	$(CC) -o $@ $(WARNINGS) $(DEBUG) $(OPTIMIZE) -J7 -std=$(CPP_VERSION) $(SOURCES)

clean:
	rm -f lab3_ex4

# Builder will call this to install the application before running.
install:
	echo "Installing is not supported"

# Builder uses this target to run your application.
run:
	./lab3_ex4
